

const crearPregunta = () => {
    const numDivs = document.getElementsByClassName("item").length;
    if (numDivs < 5) {
        const contenedor = document.getElementById("contenedor-preguntas");
        console.log(numDivs)
        let id = idPregunta();
        let template = `
            <div class="item">
                <input type="text" placeholder="${id}" id="${id}" class="estilo-input">
                <input type="button" class="eliminar boton-eliminar-pre" onClick="eliminarDiv(this)" value="Eliminar esta pregunta">
                <div id="${id}-opciones">
                    <div class="${id}-contenedor opciones-crear">    
                        <input id="${id}-op1" type="text" placeholder="opcion 1" class="estilo-input">
                        <input type="button" class="eliminar boton-eliminar-opc" onClick="eliminarDiv(this)" value="X"  >
                    </div>
                    <div class="${id}-contenedor opciones-crear">    
                        <input id="${id}-op2" type="text" placeholder="opcion 2" class="estilo-input">
                        <input type="button" class="eliminar boton-eliminar-opc" onClick="eliminarDiv(this)" value="X" >
                    </div>
                    <div class="${id}-contenedor opciones-crear">    
                        <input id="${id}-op3" type="text" placeholder="opcion 3" class="estilo-input">
                        <input type="button" class="eliminar boton-eliminar-opc" onClick="eliminarDiv(this)" value="X" >
                    </div>
                    <div class="${id}-contenedor opciones-crear">    
                        <input id="${id}-op4" type="text" placeholder="opcion 4" class="estilo-input">
                        <input type="button" class="eliminar boton-eliminar-opc" onClick="eliminarDiv(this)" value="X" >
                    </div>
                </div>
            </div>
            `
            ;

        contenedor.insertAdjacentHTML("beforeend", template);
    } else {
        alert('maximo de 5 preguntas');
    }
}

const idPregunta = () => {
    for (var i = 1; i <= 5; i++) {
        if (document.getElementById("pregunta" + i) === null) {
            return "pregunta" + i;
        }
    }
}

const eliminarDiv = (obj) => {
    const div = obj.parentElement;
    div.remove();
}

let pregunta1 = [];
let pregunta2 = [];
let pregunta3 = [];
let pregunta4 = [];
let pregunta5 = [];
const formulario = document.getElementById("form-encuesta");


const recibirData = () => {
    let titulo = formulario["titulo"].value;
    let descripcion = formulario["descripcion"].value;
    limpiarPreguntas();
    console.log("titulo: " + titulo);
    console.log("descripcion: " + descripcion);
    const numDivs = document.getElementsByClassName("item").length;
    for (let i = 1; i <= numDivs; i++) {
        console.log(i);
        let preg = formulario[`pregunta${i}`].value;
        console.log(preg);
        procesarPregunta(i)
    }
    console.log(pregunta1);
    console.log(pregunta2);
    console.log(pregunta3);
    console.log(pregunta4);
    console.log(pregunta5);

}

const procesarPregunta = (id) => {

    let lista = document.getElementById(`pregunta${id}-opciones`)
    let cantidadPreguntas = lista.getElementsByTagName("div").length
    for (let x = 1; x <= cantidadPreguntas; x++) {
        let valorPregunta = formulario[`pregunta${id}-op${x}`].value;
        if (valorPregunta != "") {
            if (id == 1) { pregunta1.push(valorPregunta); }
            else if (id == 2) { pregunta2.push(valorPregunta) }
            else if (id == 3) { pregunta3.push(valorPregunta) }
            else if (id == 4) { pregunta4.push(valorPregunta) }
            else if (id == 5) { pregunta5.push(valorPregunta) }
        }
    }
}

const limpiarPreguntas = () => {
    pregunta1.length = 0;
    pregunta2.length = 0;
    pregunta3.length = 0;
    pregunta4.length = 0;
    pregunta5.length = 0;
}

const preVista = (array) => {
    let textoOpciones = "";
    if (array.length != 0) {
        for (let opcion = 0; opcion < array.length; opcion++) {
            textoOpciones += `
                    <div class="opciones-visualizacion">
                        <input type="checkbox" name="${array[opcion]}" id="${array[opcion]}">
                        <label for="${array[opcion]}">${array[opcion]}</label>
                    </div>`;
        }
        return textoOpciones;
    }
    else {
        return `<p></p>`
    }
}
const existenciaPregunta = (resp) => {
    let elemento = document.getElementById(`pregunta${resp}`);
    if (typeof (elemento) != 'undefined' && elemento != null) {
        return `<h3>${resp}) ${elemento.value}</h3>`;
    } else {
        return `<p></p>`
    }
}
const divVista = () => {
    recibirData();
    let titulo = formulario["titulo"].value;
    let descripcion = formulario["descripcion"].value;
    // let preg2 = existenciaPregunta(formulario["pregunta2"].value);
    // let preg3 = existenciaPregunta(formulario["pregunta3"].value);
    // let preg4 = existenciaPregunta(formulario["pregunta4"].value);
    let template = `
        <h2 class="titulo">${titulo}</h2>
        <h3 class="descripcion">${descripcion}</h3>
        <div>   
            ${existenciaPregunta(1)}
            ${preVista(pregunta1)}
        </div>
        <div>
            ${existenciaPregunta(2)}
            ${preVista(pregunta2)}
        </div>
        <div>
            ${existenciaPregunta(3)}
            ${preVista(pregunta3)}
        </div>
        <div>
            ${existenciaPregunta(4)}
            ${preVista(pregunta4)}
        </div>
        <div>
            ${existenciaPregunta(5)}
            ${preVista(pregunta5)}
        </div >
        <hr class="barra-divisor">
    `;
    document.getElementById("vista").innerHTML = template;
    let visual = document.getElementById("visualizacion");
    let impresion = document.getElementById("btn-impresion");
    if (!impresion) {
        visual.insertAdjacentHTML("beforeend", `<button onclick="imprimir()" class="boton" id="btn-impresion"><i class="fas fa-print"></i> Imprimir</button>`);
    }

}


const imprimir = () => {
    let impresion = document.getElementById("vista");
    let ventanaImpresion = window.open('', 'popimpr');
    ventanaImpresion.document.write(impresion.innerHTML);
    ventanaImpresion.document.close();
    ventanaImpresion.print();
    ventanaImpresion.close();
}